#include "calculator.h"
#include <iostream>
#include <sstream>
#include <string>
using namespace std;

int main(int argc, char** argv)
{
        int first_operand;
        int second_operand;
        if (argc == 4 && istringstream(argv[1]) >> first_operand &&
            istringstream(argv[3]) >> second_operand) {
                string operation(argv[2]);
                calculator calc;
                if (operation == "+") {
                        cout << calc.add(first_operand, second_operand) << "\n";
                } else if (operation == "-") {
                        cout << calc.subtract(first_operand, second_operand)
                             << "\n";
                } else if (operation == "*") {
                        cout << calc.multiply(first_operand, second_operand)
                             << "\n";
                } else {
                        cerr << "Unsupported operation!\n";
                        return 1;
                }
        } else {
                cerr << "Usage: ./calculator <x> <operation> <y>\n";
                return 1;
        }
}
