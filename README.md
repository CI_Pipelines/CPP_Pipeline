# A GitLab CI pipeline template for C++

In the `.gitlab-ci.yml` file, we use indicative tools for the C++ programming
language in order to produce a complete Continuous Integration pipeline.

Under the _CI / CD > Pipelines_ section, there's an example of the created
pipeline.

Pipeline templates for other programming languages are
[available](https://gitlab.com/users/pothitos/projects) too.
